package ru.alekseev.tm.constant;

public class CommandConst {

    public static final String HELP = "help";
    public static final String EXIT = "exit";

    public static final String SHOW_PROJECTS = "show-projects";
    public static final String ADD_PROJECT = "add-project";
    public static final String UPDATE_PROJECT = "update-project";
    public static final String DELETE_PROJECT = "delete-project";
    public static final String CLEAR_PROJECTS = "clear-projects";

    public static final String SHOW_TASKS = "show-tasks";
    public static final String ADD_TASK = "add-task";
    public static final String UPDATE_TASK = "update-task";
    public static final String DELETE_TASK = "delete-task";
    public static final String CLEAR_TASKS = "clear-tasks";
}
