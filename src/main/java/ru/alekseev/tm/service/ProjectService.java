package ru.alekseev.tm.service;

import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.repository.ProjectRepository;

import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public void addProject(String name) {
        if (name == null || name.isEmpty()) return;
        Project project = new Project();
        project.setName(name);
        this.projectRepository.persist(project);
    }

    public List<Project> showProjects() {
        List<Project> list = this.projectRepository.findAll();
        return list;
    }

    public void updateProject(String projectID, String name) {
        if (projectID == null || projectID.isEmpty() || name == null || name.isEmpty()) return;
        Project project = new Project();
        project.setName(name);
        project.setId(projectID);
        this.projectRepository.merge(project);
    }

    public void deleteProject(String projectID) {
        if (projectID == null || projectID.isEmpty() || this.projectRepository.findOne(projectID) == null) return;
        //про findOne ok?
        this.projectRepository.remove(projectID);
    }

    public void clearProjects() {
        this.projectRepository.removeAll();
    }
}
