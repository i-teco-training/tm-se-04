package ru.alekseev.tm.service;

import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.repository.TaskRepository;

import java.util.List;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void addTask(String name, String projectID) {
        if (name == null || name.isEmpty()) return;
        Task task = new Task();
        task.setName(name);
        task.setProjectID(projectID);
        this.taskRepository.persist(task);
    }

    public List<Task> showTasks() {
        List<Task> list = this.taskRepository.findAll();
        return list;
    }

    public void updateTask(String taskID, String name) {
        if (taskID == null || taskID.isEmpty() || name == null || name.isEmpty()) return;
        Task task = new Task();
        task.setName(name);
        task.setId(taskID);
        this.taskRepository.merge(task);
    }

    public void deleteTask(String taskID) {
        if (taskID == null || taskID.isEmpty() || this.taskRepository.findOne(taskID) == null) return;
        //про findOne ok?
        this.taskRepository.remove(taskID);
    }

    public void clearTasks(String projectID) {
        List<Task> list = this.taskRepository.findAll();
        for (Task task : list) {
            if (projectID.equals(task.getProjectID())) {
                this.taskRepository.remove(task.getId());
            }
        }
    }
}
