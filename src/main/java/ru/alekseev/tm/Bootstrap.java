package ru.alekseev.tm;

import ru.alekseev.tm.constant.CommandConst;
import ru.alekseev.tm.entity.Project;
import ru.alekseev.tm.entity.Task;
import ru.alekseev.tm.repository.ProjectRepository;
import ru.alekseev.tm.repository.TaskRepository;
import ru.alekseev.tm.service.ProjectService;
import ru.alekseev.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

public class Bootstrap {

    private ProjectRepository projectRepository = new ProjectRepository();
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskRepository taskRepository = new TaskRepository();
    private TaskService taskService = new TaskService(taskRepository);
    private static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public void init() throws Exception { //надо поймать Exception(?)

        System.out.println("*** WELCOME TO TASK MANAGER ***");

        while (true) {
            String input = reader.readLine();
            switch (input) {
                case CommandConst.HELP:
                    showCommands();
                    break;
                case CommandConst.ADD_PROJECT:
                    addProject();
                    break;
                case CommandConst.SHOW_PROJECTS:
                    showProjects();
                    break;
                case CommandConst.UPDATE_PROJECT:
                    updateProject();
                    break;
                case CommandConst.DELETE_PROJECT:
                    deleteProject();
                    break;
                case CommandConst.CLEAR_PROJECTS:
                    clearProjects();
                    break;
                case CommandConst.ADD_TASK:
                    addTask();
                    break;
                case CommandConst.SHOW_TASKS:
                    showTasks();
                    break;
                case CommandConst.UPDATE_TASK:
                    updateTask();
                    break;
                case CommandConst.DELETE_TASK:
                    deleteTask();
                    break;
                case CommandConst.CLEAR_TASKS:
                    clearTasks();
                    break;
                case CommandConst.EXIT:
                    System.exit(0);
                    break;
                default:
                    showCommands();
                    break;
            }
            System.out.println();
        }
    }

    private static void showCommands() {
        System.out.println(CommandConst.HELP + " - Show all commands");

        System.out.println(CommandConst.SHOW_PROJECTS + " - Show all projects");
        System.out.println(CommandConst.ADD_PROJECT + " - Add new project");
        System.out.println(CommandConst.UPDATE_PROJECT + " - Update project");
        System.out.println(CommandConst.DELETE_PROJECT + " - Delete project by number");
        System.out.println(CommandConst.CLEAR_PROJECTS + " - Delete all projects");

        System.out.println(CommandConst.SHOW_TASKS + " - Show all tasks");
        System.out.println(CommandConst.ADD_TASK + " - Add new task");
        System.out.println(CommandConst.CLEAR_TASKS + " - Delete all tasks");

        System.out.println(CommandConst.EXIT + " - Stop the Project Manager Application");
    }

    private void addProject() throws Exception {
        System.out.println("[ADDING OF NEW PROJECT]");
        System.out.println("ENTER NAME");
        String name = reader.readLine();
        projectService.addProject(name);
        System.out.println("[OK]");
    }

    private void showProjects() {
        System.out.println("[LIST OF ALL PROJECTS]");
        List<Project> list = projectService.showProjects();
        if (list.size() == 0) {
            System.out.println("LIST OF PROJECTS IS EMPTY");
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + list.get(i).getName() + ", projectID: " + list.get(i).getId());
        }
    }

    private void updateProject() throws Exception {
        System.out.println("[UPDATING OF PROJECT]");
        System.out.println("ENTER PROJECT ID");
        String projectID = reader.readLine();
        System.out.println("ENTER NEW NAME");
        String newName = reader.readLine();
        projectService.updateProject(projectID, newName);
        System.out.println("[OK]");
    }

    private void deleteProject() throws Exception {
        System.out.println("[DELETING OF PROJECT]");
        System.out.println("ENTER PROJECT ID");
        String projectID = reader.readLine();
        projectService.deleteProject(projectID);
        taskService.clearTasks(projectID);
        System.out.println("[OK. PROJECT AND IT'S TASKS DELETED]");
    }

    private void clearProjects() {
        List<Project> list = projectService.showProjects();
        for (Project project : list) {
            taskService.clearTasks(project.getId());
        }
        projectService.clearProjects();
        System.out.println("[OK. ALL PROJECTS AND THEIR TASKS DELETED]");
    }

    private void addTask() throws Exception {
        System.out.println("[ADDING OF NEW TASK]");
        System.out.println("TYPE \"1\" TO ATTACH NEW TASK TO EXISTING PROJECT (otherwise press \"ENTER\" key)");
        String userChoice = reader.readLine();
        String projectID = null;
        if ("1".equals(userChoice)) {
            System.out.println("ENTER PROJECT ID");
            projectID = reader.readLine();
        }
        System.out.println("ENTER TASK NAME");
        String name = reader.readLine();
        taskService.addTask(name, projectID);
        System.out.println("[OK]");
    }

    private void showTasks() {
        System.out.println("[LIST OF ALL TASKS]");
        List<Task> list = taskService.showTasks();
        if (list.size() == 0) {
            System.out.println("LIST OF TASKS IS EMPTY");
        }
        for (int i = 0; i < list.size(); i++) {
            System.out.print(i + 1);
            System.out.println(") name:" + list.get(i).getName() + ", taskID: " + list.get(i).getId() + ", projectID: " + list.get(i).getProjectID());
        }
    }

    private void updateTask() throws Exception {
        System.out.println("[UPDATING OF TASK]");
        System.out.println("ENTER TASK ID");
        String taskID = reader.readLine();
        System.out.println("ENTER NEW NAME");
        String newName = reader.readLine();
        taskService.updateTask(taskID, newName);
        System.out.println("[OK]");
    }

    private void deleteTask() throws Exception {
        System.out.println("[DELETING OF TASK]");
        System.out.println("ENTER TASK ID");
        String taskID = reader.readLine();
        taskService.deleteTask(taskID);
        System.out.println("[OK]");
    }

    private void clearTasks() throws Exception {
        System.out.println("[CLEARING OF TASKS BY PROJECT]");
        System.out.println("ENTER PROJECT ID");
        String projectID = reader.readLine();
        taskService.clearTasks(projectID);
        System.out.println("[ALL TASKS ATTACHED TO PROJECT (projectID = " + projectID + ") DELETED]");
    }
}