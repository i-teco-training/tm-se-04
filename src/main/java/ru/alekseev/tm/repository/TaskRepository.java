package ru.alekseev.tm.repository;

import ru.alekseev.tm.entity.Task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository {
    private Map<String, Task> tasks = new HashMap<>();

    public List<Task> findAll() {
        List<Task> list = new ArrayList<>(tasks.values());
        return list;
    }

    public Task findOne(String taskID) {
        return tasks.get(taskID);
    }

    public void persist(Task task) {
        if (!tasks.containsKey(task.getId())) {
            tasks.put(task.getId(), task);
        }
    }

    public void merge(Task task) {
        tasks.put(task.getId(), task);
    }

    public void remove(String taskID) {
        tasks.remove(taskID);
    }
}
